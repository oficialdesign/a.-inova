<?php include('header.php'); ?>

	<section id="dz_main_slider">
		<!-- START REVOLUTION SLIDER 4.6.0 fullwidth mode -->
		<div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:420px;">
			<div id="rev_slider_3_1" class="rev_slider fullwidthabanner" style="display:none;max-height:420px;height:420px;">
				<ul>	<!-- SLIDE  -->
					<li data-transition="parallaxtoleft" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
						<!-- MAIN IMAGE -->
							<img src="slider-images/slider1.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<!-- LAYERS -->
					</li>
					<!-- SLIDE  -->
					<li data-transition="slidedown" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
						<!-- MAIN IMAGE -->
							<img src="slider-images/transparent.png" style='background-color:#202024' alt="bg2"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
						<!-- LAYERS -->
						<!-- LAYER NR. 1 -->
						<div class="tp-caption lfr rs-parallaxlevel-1" data-x="-224" data-y="0"  data-speed="2300" data-start="0" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2;">
							<img src="slider-images/balls-back.png" alt="">
						</div>
						<!-- LAYER NR. 2 -->
						<div class="tp-caption lft stl rs-parallaxlevel-0" data-x="415" data-y="92"  data-speed="1000" data-start="1050" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 3;">
							<img src="slider-images/hint.png" alt="+box">
						</div>
						<!-- LAYER NR. 3 -->
						<div class="tp-caption shard_slider_big sfl tp-resizeme rs-parallaxlevel-0" data-x="455" data-y="152"  data-speed="1000" data-start="1100" data-easing="Power3.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0" data-endelementdelay="0.1" data-endspeed="1000" style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
							the COLOR OF YOU
						</div>
						<!-- LAYER NR. 4 -->
						<div class="tp-caption shard_small sfl tp-resizeme rs-parallaxlevel-0" data-x="667" data-y="223"  data-speed="1000" data-start="1300" data-easing="Power3.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">
							Easily apply your Brand Identity!
						</div>
						<!-- LAYER NR. 5 -->
						<div class="tp-caption shard_smaller_right lfb tp-resizeme rs-parallaxlevel-0" data-x="716" data-y="291"  data-speed="600" data-start="1500" data-easing="Power3.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6; max-width: 439px; max-height: 40px; white-space: nowrap;">
							Shard is cleverly designed with simple and easy customization<br>in mind. It features one single main color that controls the<br>whole look and feel. Change it into your Brand color and<br>you are good to go. Simple...
						</div>
						<!-- LAYER NR. 6 -->
						<div class="tp-caption lfr ltl rs-parallaxlevel-10" data-x="-154" data-y="-24"  data-speed="1900" data-start="400" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="1000" data-endeasing="Power1.easeOut" style="z-index: 7;">
							<img src="slider-images/Untitled-4-1.png" alt="">
						</div>
						<!-- LAYER NR. 7 -->
						<div class="tp-caption lfr rs-parallaxlevel-3" data-x="right" data-hoffset="50" data-y="bottom" data-voffset="50" data-speed="1500" data-start="800" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 8;">
							<img src="slider-images/Untitled-6-1.png" alt="">
						</div>
					</li>
					<!-- SLIDE  -->
					<li data-transition="parallaxtoright" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
						<!-- MAIN IMAGE -->
							<img src="slider-images/transparent.png" style='background-color:#5f90b6' alt="bg3"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
						<!-- LAYERS -->
						<!-- LAYER NR. 1 -->
						<div class="tp-caption lfr ltb rs-parallaxlevel-1" data-x="375" data-y="5"  data-speed="1300" data-start="300" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="900" style="z-index: 2;">
							<img src="slider-images/0-1.png" alt="">
						</div>
						<!-- LAYER NR. 2 -->
						<div class="tp-caption lfr ltb rs-parallaxlevel-1" data-x="795" data-y="bottom" data-voffset="5" data-speed="1300" data-start="700" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="900" style="z-index: 3;">
							<img src="slider-images/Untitled-4-2.png" alt="">
						</div>
						<!-- LAYER NR. 3 -->
						<div class="tp-caption lft ltt rs-parallaxlevel-1" data-x="418" data-y="5"  data-speed="1300" data-start="800" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="900" style="z-index: 4;">
							<img src="slider-images/4-1.png" alt="">
						</div>
						<!-- LAYER NR. 4 -->
						<div class="tp-caption lft ltt rs-parallaxlevel-10" data-x="418" data-y="-12"  data-speed="1300" data-start="1100" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="900" style="z-index: 5;">
							<img src="slider-images/5-1.png" alt="">
						</div>
						<!-- LAYER NR. 5 -->
						<div class="tp-caption lft ltt rs-parallaxlevel-2" data-x="375" data-y="-15"  data-speed="1300" data-start="1000" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="900" style="z-index: 6;">
							<img src="slider-images/Untitled-6-2.png" alt="">
						</div>
						<!-- LAYER NR. 6 -->
						<div class="tp-caption lft ltt rs-parallaxlevel-3" data-x="375" data-y="-20"  data-speed="1300" data-start="1300" data-easing="Power3.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="900" style="z-index: 7;">
							<img src="slider-images/Untitled-7-1.png" alt="">
						</div>
						<!-- LAYER NR. 7 -->
						<div class="tp-caption lft stl rs-parallaxlevel-0" data-x="30" data-y="92"  data-speed="600" data-start="1200" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 8;">
							<img src="slider-images/hint.png" alt="+box">
						</div>
						<!-- LAYER NR. 8 -->
						<div class="tp-caption shard_slider_big sfl tp-resizeme rs-parallaxlevel-0" data-x="70" data-y="152"  data-speed="1000" data-start="1250" data-easing="Power3.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0" data-endelementdelay="0.1" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
							BUILD INTUITIVELY!
						</div>
						<!-- LAYER NR. 9 -->
						<div class="tp-caption shard_small sfl tp-resizeme rs-parallaxlevel-0" data-x="70" data-y="223"  data-speed="1000" data-start="1450" data-easing="Power3.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">
							Drag, Drop, Done!
						</div>
						<!-- LAYER NR. 10 -->
						<div class="tp-caption black sfb tp-resizeme rs-parallaxlevel-0" data-x="70" data-y="287"  data-speed="1500" data-start="2000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 11; max-width: auto; max-height: auto; white-space: nowrap;">
							<a href="#" target="_blank" class="shard-button shard-button_red shard-button_small ">
								See it in Action!
							</a>
						</div>
					</li>
				</ul>
				<div class="tp-bannertimer tp-bottom"></div>
			</div>
		</div>
		<!-- END REVOLUTION SLIDER -->
	</section>

	<section class="shard_section_dd ">
		<div class="shard_section_content">
			<div class="shard_container">
				<header>
					<h3 class="margin_bottom80">Serviços e Soluções</h3>
				</header>
			</div>
			<div class="shard_container">
				<div class="box-servicos shard_column_dd_span6 ">
					<div class="shard_column_dd_span4 aligncenter">
						<div class="icone"><img src="images/icone-consultoria.png"></div>
						<h4>Assessoria e Consultoria</h4>
					</div>
					<div class="shard_column_dd_span7 ">
						<p>A A.Inova oferece consultoria e assessoria nas áreas de Processo de desenvolvimento e serviços, Inovação, Usabilidade e Estratégia de Negócio, focando no  apoio de seus clientes na melhoria de processos organizacionais, aumentando a qualidade de seus produtos e serviços e na entrega de valor a seus clientes/usuários.</p>
						<a href="#" class="shard-button shard-button_blue shard-button_medium">Conheça os cursos <i class="icon-arrow-right2"></i></a>
					</div>
					<div class="shard_column_dd_span1 "></div>
				</div>
				<div class="box-servicos shard_column_dd_span6 ">
					<div class="shard_column_dd_span1 "></div>
					<div class="shard_column_dd_span4 aligncenter">
						<div class="icone"><img src="images/icone-treinamento.png"></div>
						<h4>Treinamentos</h4>
					</div>
					<div class="shard_column_dd_span7 ">
						<p>A A.Inova valoriza e incentiva o conhecimento, oferecendo treinamentos que promovam o crescimento individual e organizacional, além de treinamentos periódicos em instalações terceirizadas e próprias, a A.Inova oferece a aplicação In Company, customizados para seus clientes, alinhados a suas necessidades, contando com parceiros em diferentes regiões do país.</p>
						<a href="#" class="shard-button shard-button_blue shard-button_medium">Conheça os cursos <i class="icon-arrow-right2"></i></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="shard_section_dd padding_reduced_both">
		<div class="shard_section_content">
			<div class="shard_container">
				<div class="shard_column_dd_span3 ">
					<div class="shard-animo " data-animation="bounceInLeft" data-duration="500" data-delay="800">
						<img src="images/logo-nti.jpg" alt="">
					</div>
				</div>
				<div class="shard_column_dd_span3 ">
					<div class="shard-animo " data-animation="bounceInLeft" data-duration="500" data-delay="400">
						<img src="images/logo-sebrae.jpg" alt="">
					</div>
				</div>
				<div class="shard_column_dd_span3 ">
					<div class="shard-animo " data-animation="bounceInLeft" data-duration="500" data-delay="0">
						<img src="images/logo-pato-branco.jpg" alt="">
					</div>
				</div>
				<div class="shard_column_dd_span3 ">
					<div class="shard-animo " data-animation="bounceInRight" data-duration="500" data-delay="200">
						<img src="images/logo-wise.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>