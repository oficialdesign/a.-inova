<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>A. Inova - Consultores Assossiados</title>
	<meta name="description" content="Shard HTML5 premium template">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" type="text/css">
	<link rel="stylesheet" href="css/animo-animate.css" type="text/css" media="all">
	<link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/revolution_captions.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/icomoon.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="all">
	<link rel='stylesheet' href="css/jquery.fancybox-1.3.4.css" type="text/css" media="all" />
	<link rel='stylesheet' href="css/nivo-slider.css" type="text/css" media="all" />
	<link rel='stylesheet' href="css/owl.carousel.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/custom.css" type="text/css" media="all">
</head>

<body>
	<div id="topbar_and_header" class="th_style_1 ">
		<div id="ABdev_topbar">
			<div class="container">
				<div class="row">
					<div id="header_phone_email_info" class="span8">
						<span><i class="icon-phone"></i>+55 46 8402 4898</span>
						<span>
							<i class="icon-emailalt"></i>
							<a href="mailto:info@shard.com">contato@ainovaconsultores.com.br</a>
						</span>
					</div>
					<div id="header_social_search" class="span4 right_aligned">
						<a href="#" class="social_link shard_tooltip" data-gravity="n" target="_blank" title="Facebook"><i class="icon-facebook"></i></a>
						<a href="#" class="social_link shard_tooltip" data-gravity="n" target="_blank" title="Linkedin"><i class="icon-linkedin"></i></a>
						<a href="#" class="social_link shard_tooltip" data-gravity="n" target="_blank" title="Twitter"><i class="icon-twitter"></i></a>
						<div id="ABdev_menu_search">
							<form name="search" method="get" action="index.html">
								<input name="s" type="text" placeholder="Procure no site" value="">
								<a class="submit"><i class="icon-search"></i></a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<header id="ABdev_main_header" class="clearfix">
			<div class="container">
				<div id="logo">
					<div class="logo_inner">
						<a href="index.php">
							<img src="images/logo.png" alt="Shard">
						</a>
					</div>
				</div>
				<nav>
					<ul id="main_menu">
						<li id="magic-line"></li>
						<li class="current-menu-item"><a href="#"><span>Institucional</span></a>
							<ul>
								<li><a href="institucional.php"><span>Sobre nós</span></a></li>
								<li><a href="equipe.php"><span>Equipe</span></a></li>
							</ul>
						</li>
						<li><a href="#"><span>Serviços e Soluções</span></a>
							<ul>
								<li><a href="assessoria-consultoria.php"><span>Assessoria e Consultoria</span></a></li>
								<li><a href="treinamentos.php"><span>Treinamentos</span></a></li>
								<li><a href="treinamentos-terceirizados.php"><span>Treinamentos Terceirizados</span></a></li>
							</ul>
						</li>
						<li><a href="parceiros.php"><span>Parceiros</span></a></li>
						<li><a href="noticias.php"><span>Notícias</span></a></li>
						<li><a href="depoimentos.php"><span>Depoimentos</span></a></li>
						<li><a href="atendimento.php"><span>Atendimento</span></a></li>
					</ul>
				</nav>
				<div id="ABdev_menu_toggle">
					<i class="icon-menumobile"></i>
				</div>
			</div>
		</header>
	</div>