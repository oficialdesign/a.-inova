<?php include('header.php'); ?>

	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="tbb_title">
				<h1>Atendimento</h1>
			</div>
			<div class="tbb_breadcrumbs">
				<div class="container">
					<div class="breadcrumbs">
						<div class="breadcrumbs_inner">Você está em:
							<a href="index.html">Inicial</a>/
							<span class="current">Atendimento</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="shard_section_dd no_padding_bottom contact_section_alternative">
		<div class="shard_section_content">
			<div class="shard_container">
				<div class="shard_column_dd_span9 ">
					<h2><span>Envie uma mensagem pra gente</span></h2>
					<p>Em tudo o que faz, a A . Inova é movida por seu compromisso com o sucesso.</p>
					<p>Use este espaço para fazer comentários ou perguntas sobre nossa empresa ou nossos produtos.</p>
					<div class="contact" id="contact-wrapper" dir="ltr">
						<form action="#" method="post" class="contact-form">
							<div class="hidden">
								<input type="hidden" name="nonce" value="a091ecdcf1">
								<input type="hidden" name="formid" id="formid" value="contact">
							</div>
							<div class="row">
								<div class="span5">
									<span class="your-name">
										<input type="text" name="name" size="40" class="contact-text" placeholder="Nome">
									</span>
									<span class="your-email">
										<input type="email" name="email" size="40" class="contact-text contact-email contact-validates-as-email" placeholder="E-mail">
									</span>
									<span class="your-subject">
										<input type="text" name="subject" size="40" class="contact-text" placeholder="Assunto">
									</span>
								</div>
								<div class="span7">
									<p>
										<span class="your-message">
											<textarea name="message" cols="40" rows="10" class="contact-textarea" placeholder="Mensagem"></textarea>
										</span>
									</p>
								</div>
							</div>
							<div class="right_aligned">
								<input type="submit" value="Enviar" class="contact-submit" id="contact-submit">
							</div>
						</form>
						<div class="contact-response-output contact-display-none"></div>
					</div>

				</div>
				<div class="shard_column_dd_span3 ">
					<h2><span>Contatos</span></h2>
					<strong>Telefone</strong>
					<p class="contact_page_info">(46) <strong>3025-6866</strong></p>
					<p><strong>Endereço</strong></p>
					<p class="contact_page_info">Rua Iguaçu, 605 - Sala 03<br> Pato Branco, PR</p>
					<p><strong>E-mail</strong></p>
					<p class="contact_page_info">
						<a href="#">fabio@ainovaconsultores.com.br</a>
					</p>
					<p><strong>Skype</strong></p>
					<p class="contact_page_info">a.inova</p>
				</div>
			</div>
		</div>
	</section>

	<section class="shard_section_dd">
		<div class="shard_section_content">
			<div class="shard_container">
				<div class="shard_column_dd_span12">
					<div id="shard_google_map_1"
					data-map_type="ROADMAP"
					data-lat="45.4385847"
					data-lng="12.3284471"
					data-zoom="17"
					data-scrollwheel="0"
					data-maptypecontrol="1"
					data-pancontrol="1"
					data-zoomcontrol="1"
					data-scalecontrol="1"
					data-markertitle="A. Inova"
					data-markericon="images/map-pointer.png"
					data-markercontent="Our Address"
					data-markerlat="45.4385847"
					data-markerlng="12.3284471"
					class="shard_google_map" style="height:570px;width:100%;">
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>