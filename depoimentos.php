<?php include('header.php'); ?>

	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="tbb_title">
				<h1>Depoimentos</h1>
			</div>
			<div class="tbb_breadcrumbs">
				<div class="container">
					<div class="breadcrumbs">
						<div class="breadcrumbs_inner">Você está em:
							<a href="index.html">Inicial</a>/
							<span class="current">Depoimentos</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="span6">
					<div class="box-depoimento">
						<div class="box">
							<p>Quinze minutos com um consultor da A.Inova, muitas vezes valem por 15 anos de carreira!</p>
						</div>
						<h5>João Pinheiros</h5>
					</div>
				</div>
				<div class="span6">
					<div class="box-depoimento">
						<div class="box">
							<p>O sucesso da A.Inova é fruto de um trabalho baseado na ética, conhecimento do modelo e compromisso com o cliente.</p>
						</div>
						<h5>Wagner Fernando Rodello</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="span6">
					<div class="box-depoimento">
						<div class="box">
							<p>Gostaria de, em nome de toda a nossa equipe, agradecer à A.Inova pelo apoio, orientações e também pela condução do processo de aprimoramento do meu negócio.</p>
						</div>
						<h5>João Pinheiros</h5>
					</div>
				</div>
				<div class="span6">
					<div class="box-depoimento">
						<div class="box">
							<p>Agradeço à A.Inova pelo suporte e os parabenizo pela competência demonstrada durante todo o trabalho.</p>
						</div>
						<h5>Wagner Fernando Rodello</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="span12">
					<section id="blog_pagination" class="clearfix">
						<div class="pagination noborder">
							<span class="blog_pagexofy left">Page 1 of 2</span>
							<span class="page-numbers current">1</span>
							<a class="page-numbers" href="blog-right-sidebar.html">2</a> 
							<a class="next page-numbers" href="blog-right-sidebar.html"><i class="icon-chevronright"></i></a>
						</div>
					</section>
				</div>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>