<aside class="span3 sidebar sidebar_right">
	<div class="widget widget_search">
		<div class="sidebar-widget-heading">
			<h5>Assine nossa Newsletter</h5>
		</div>
		<div class="widget_search">
			<p>Cadastre-se e receba novidades com exclusividade.</p>
			<form name="search" id="search" method="get" action="#">
				<input name="s" type="text" placeholder="Informe seu e-mail" value="">
				<a class="submit">
					<i class="icon-paperplane"></i>
				</a>
			</form>
		</div>
	</div>
	<div class="widget widget_tag_cloud">
		<div class="sidebar-widget-heading">
			<h5>Tags</h5>
		</div>
		<div class="tagcloud">
			<a href="interna-noticias.php" title="3 topics">ab-themes</a>
			<a href="interna-noticias.php" title="9 topics">art</a>
			<a href="interna-noticias.php" title="8 topics">blog</a>
			<a href="interna-noticias.php" title="8 topics">CSS3</a>
			<a href="interna-noticias.php" title="9 topics">design</a>
			<a href="interna-noticias.php" title="2 topics">desing</a>
			<a href="interna-noticias.php" title="7 topics">development</a>
			<a href="interna-noticias.php" title="7 topics">fontawesome</a>
			<a href="interna-noticias.php" title="9 topics">HTML5</a>
			<a href="interna-noticias.php" title="9 topics">innovation</a>
			<a href="interna-noticias.php" title="7 topics">responsive</a>
			<a href="interna-noticias.php" title="1 topic" >theme</a>
			<a href="interna-noticias.php" title="6 topics">themeforest</a>
			<a href="interna-noticias.php" title="5 topics">wordpress</a>
		</div>
	</div>
</aside>