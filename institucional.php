<?php include('header.php'); ?>

	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="tbb_title">
				<h1>Sobre a A.INova Consultores Associados</h1>
			</div>
			<div class="tbb_breadcrumbs">
				<div class="container">
					<div class="breadcrumbs">
						<div class="breadcrumbs_inner">Você está em:
							<a href="index.html">Inicial</a>/
							<span class="current">Institucional</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="img-top">
		<div class="shard_container">
			<img src="images/img-institucional.jpg" alt="">
		</div>
	</section>
	<section class="shard_section_dd">
		<div class="shard_container">
			<div class="shard_column_dd_span5">
				<h3>Sobre nós</h3>
				<p>A A.Inova Consultores Associados é uma empresa, com sede em Pato Branco – PR, atua no Paraná e Santa Catarina, é composta por equipe multidisciplinar com mais de 10 anos de experiência na área de atuação, prima pela qualidade de seus serviços oferecidos, alinhados a interesses de seus clientes e resultados que gerem valor. </p>
			</div>
			<div class="shard_column_dd_span2"></div>
			<div class="shard_column_dd_span5">
				<div class="shard_section_dd no_padding margin_bottom">
					<h3>Missão</h3>
					<p>Promover o crescimento de organizações e pessoas, através de informação e resultados que gerem valor, tornando nossos clientes mais competitivos.</p>
				</div>
				<div>
					<h3>Visão</h3>
					<p>Ser referencia em consultoria e assessoria de estratégia de negócios, inovação e processos organizacionais.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="shard_section_dd">
		<div class="shard_container">
			<div class="shard_column_dd_span7">
				<h3>Valores</h3>
				<ul class="list">
					<li>Ações éticas através de processo organizacional transparente que valorize o ser humano;</li>
					<li>Valorizar ações de cooperação, iniciativas e construção de alianças institucionais;</li>
					<li>Respeito a diferentes opiniões, mantendo fluxo de comunicações e canais de diálogos ativos;</li>
					<li>Compromisso com a qualidade e competência.</li>
				</ul>
			</div>
			<div class="shard_column_dd_span5">
				<div>
					<h3>Princípios</h3>
					<ul class="list">
						<li>Excelência no atendimento a clientes;</li>
						<li>Busca constante por conhecimento para melhoria dos serviços oferecidos;</li>
						<li>Rapidez e determinação para o alcance eficaz de resultados;</li>
						<li>Transparência e responsabilidade no planejamento e execução dos serviços, buscando sempre a satisfação dos clientes;</li>
						<li>Inovar, estando em sintonia com novas tecnologias de informação, contribuindo para a realização organizacional e profissional.</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="shard_section_content">
			<div class="shard_container aligncenter padding_reduced_both">
				<a href="#" target="_blank" class="shard-button shard-button_blue shard-button_medium icon-left">
					<i class="icon-arrow-left2"></i> Voltar
				</a>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>