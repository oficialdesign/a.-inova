<?php include('header.php'); ?>

	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="tbb_title">
				<h1>Nossa Equipe</h1>
			</div>
			<div class="tbb_breadcrumbs">
				<div class="container">
					<div class="breadcrumbs">
						<div class="breadcrumbs_inner">Você está em:
							<a href="index.html">Inicial</a>/
							<span class="current">Nossa Equipe</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="shard_section_dd">
		<div class="shard_container">
			<div class="shard_column_dd_span2"></div>
			<div class="shard_column_dd_span8 aligncenter">
				<p>Somos uma empresa apaixonada por disseminar conhecimento. Trabalhamos pela coletividade, promovendo bem-estar e autoestima para que as pessoas sejam mais realizadas a cada novo dia. </p>
			</div>
		</div>
		<div class="shard_container">
			<div class="box-equipe">
				<img src="images/equipe-1.jpg" alt="">
				<h4>Carlos Miguel</h4>
				<span>Consultor</span>
			</div>
			<div class="box-equipe">
				<img src="images/equipe-1.jpg" alt="">
				<h4>Carlos Miguel</h4>
				<span>Consultor</span>
			</div>
			<div class="box-equipe">
				<img src="images/equipe-1.jpg" alt="">
				<h4>Carlos Miguel</h4>
				<span>Consultor</span>
			</div>
			<div class="box-equipe">
				<img src="images/equipe-1.jpg" alt="">
				<h4>Carlos Miguel</h4>
				<span>Consultor</span>
			</div>
			<div class="box-equipe">
				<img src="images/equipe-1.jpg" alt="">
				<h4>Carlos Miguel</h4>
				<span>Consultor</span>
			</div>
			<div class="box-equipe">
				<img src="images/equipe-1.jpg" alt="">
				<h4>Carlos Miguel</h4>
				<span>Consultor</span>
			</div>
		</div>
		<div class="shard_section_content">
			<div class="shard_container aligncenter padding_reduced_both">
				<a href="javascript:history.go(-1);" class="shard-button shard-button_blue shard-button_medium icon-left">
					<i class="icon-arrow-left2"></i> Voltar
				</a>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>