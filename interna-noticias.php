<?php include('header.php'); ?>

	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="tbb_title">
				<h1>Notícias</h1>
			</div>
			<div class="tbb_breadcrumbs">
				<div class="container">
					<div class="breadcrumbs">
						<div class="breadcrumbs_inner">Você está em:
							<a href="index.html">Inicial</a>/
							<span class="current">Notícias</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="blog_category_index span9 content_with_right_sidebar">
					<div class="post_content">
						<div class="post_badges_single">
							<i class="icon-pen"></i>
							<span class="post_date">
								<span class="post_day">01 Oct</span>
								<span class="post_our_minute">12:03</span>
								<span class="post_am_pm">pm</span>
							</span>
						</div>
						<div class="post post_main post_main_alternative">
							<img width="1170" height="628" src="images/post11.jpg" alt="post1">
							<div class="postmeta_under_image ">
								<div class="author_and_categories_badges">
									<span class="posted_by_author">Publicado por <strong>A. Inova</strong></span>
								</div>
								<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <strong>bibendum auctor</strong>, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
									<span id="more-2974"></span>
									<br>
									Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
								</p>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. 
								</p>
								<blockquote class="shard_blockquote">
									<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec.
									</p>
								</blockquote>
								<p>This is Photoshop’s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. </p>
								<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeo Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. 
								</p>
								<div class="postmeta_under_text_tags">
									Tags: 
									<a href="blog-right-sidebar.html" rel="tag">art</a>, 
									<a href="blog-right-sidebar.html" rel="tag">CSS3</a>, 
									<a href="blog-right-sidebar.html" rel="tag">wordpress</a>									
								</div>
								<div class="postmeta-under clearfix">
									<p class="post_meta_share">
										<a class="post_share_facebook" href="#"><i class="icon-facebook"></i>Share</a>
										<a class="post_share_twitter" href="#"><i class="icon-twitter"></i>Tweet</a>
										<a class="post_share_googleplus" href="#"><i class="icon-googleplus"></i>Share</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php include('sidebar.php'); ?>
			</div>
			<div class="row">
				<div class="span12 aligncenter">
					<a href="javascript:history.go(-1);" class="shard-button shard-button_blue shard-button_medium icon-left">
						<i class="icon-arrow-left2"></i> Voltar
					</a>
				</div>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>