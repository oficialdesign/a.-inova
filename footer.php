	<section class="shard_section_dd no_padding newsletter">
		<div class="shard_section_content">
			<div class="shard_container">
				<div class="shard_column_dd_span12 ">
					<div class="shard-callout_box ">
						<div class="shard_container">
							<div class="shard_column_dd_span9">
								<div class="icon">
									<img src="images/icon-newsletter.png" alt="">
								</div>
								<span class="shard-callout_box_title">Quer receber novidades exclusivas?</span>
								<p>Ao cadastrar-se, você passa a receber nossos informativos, newsletter e divulgação de eventos.<br>
								<strong>É a melhor maneira de ficarmos em contato!</strong></p>
							</div>
							<div class="shard_column_dd_span3">
								<a href="#" target="_blank" class="shard-button shard-button_blue_light shard-button_small">
									CADASTRE-SE
									<i class="icon-arrow-right2"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer id="main_footer">
		<div id="footer_columns">
			<div class="container">
				<div class="row">
					<div class="span2 clearfix">
						<div class="widget widget_nav_menu">
							<h4>Mapa do Site</h4>
							<div>
								<ul>
									<li><a href="#">Institucional</a></li>
									<li><a href="#">Serviços e Soluções</a></li>
									<li><a href="#">Depoimentos</a></li>
									<li><a href="#">Parceiros</a></li>
									<li><a href="#">Atendimento</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="span4 clearfix">
						<div class="widget">
							<h4>Facebook Oficial</h4>
							<div>
								<div class="fb-page" data-href="https://www.facebook.com/ainovaconsultores" data-width="100%" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/ainovaconsultores"><a href="https://www.facebook.com/ainovaconsultores">A.Inova Consultores</a></blockquote></div></div>
							</div>
						</div>
					</div>
					<div class="span3 clearfix">
						<div id="contact-info-2" class="widget">
							<h4>Contato</h4>
							<div class="contact_info_widget">
								<div>
									Tel: <strong>+55 46 8402 4898</strong>
								</div>
								<div class="contact_info_widget_email">
									E-mail: <a href="mailto:contato@ainovaconsultores.com.br"><strong>contato@ainovaconsultores.com.br</strong></a>
								</div>
								<div>
									Rua Pedro Vieira nº 260<br>
									Bairro Bortot.<br>
									Condomínio Empresarial do NTI,<br>
									CEP: 85504-140<br>
									Pato Branco – Paraná.
								</div>
							</div>
						</div>
					</div>
					<div class="span3 clearfix">
						<div class="widget">
							<div>
								<p>Auxiliamos empreendedores a transformarem sonhos em negócio, contribuindo para que estejam equiparados a organizações que valorizem seu potencial.</p>
								<a href="#" class="shard-button shard-button_blue shard-button_small">ENVIE SEU CURRÍCULO <i class="icon-arrow-right2"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer_copyright" class="clearfix">
			<div class="container">
				<div id="footer_copyright_text">
					© Copyright by E-MID.com.br
				</div>
				<div id="footer_logo">
					<div class="footer_logo_inner">
						<a href="index.html">
							<img src="images/logo-footer.png" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="js/rev_slider_init.js"></script>
	<script type="text/javascript" src="js/mediaelement.js"></script>
	<script type="text/javascript" src="js/mediaelement-and-player.js"></script>
	<script type="text/javascript" src="js/mediaelementplayer.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.js"></script>
	<script type="text/javascript" src="js/jquery.carouFredSel-6.2.1.js"></script>
	<script type="text/javascript" src="js/ab-tweet-scroller.js"></script>
	<script type='text/javascript' src='js/ab-simple-subscribe.js'></script>
	<script type="text/javascript" src="js/animo.js"></script>
	<script type="text/javascript" src="js/jquery.inview.js"></script>
	<script type='text/javascript' src='js/jquery.nivo.slider.js'></script>
	<script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
	<script type='text/javascript' src='js/jquery.knob.js'></script>
	<script type="text/javascript" src="js/jquery.knob-custom.js"></script>
	<script type="text/javascript" src="js/jquery.ui.core.min.js"></script>
	<script type="text/javascript" src="js/jquery.ui.widget.min.js"></script>
	<script type="text/javascript" src="js/jquery.ui.accordion.min.js"></script>
	<script type="text/javascript" src="js/jquery.ui.tabs.min.js"></script>
	<script type="text/javascript" src="js/jquery.ui.effect.min.js"></script>
	<script type="text/javascript" src="js/jquery.ui.effect-slide.min.js"></script>
	<script type='text/javascript' src='js/isotope.pkgd.min.js'></script>
	<script type="text/javascript" src="js/superfish.js"></script>
	<script type="text/javascript" src="js/masonry.min.js"></script>
	<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
	<script type="text/javascript" src="js/jpreloader.js"></script>
	<script type='text/javascript' src='js/jquery.fancybox-1.3.4.js'></script>
	<script type="text/javascript" src="js/waypoints.js"></script>
	<script type="text/javascript" src="js/init.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	<script type='text/javascript' src='js/jquery.gmap.min.js'></script>
	<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&amp;ver=4.0.1'></script>

</body>
</html>