<?php include('header.php'); ?>

	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="tbb_title">
				<h1>Estratégia de Negócio</h1>
				<div id="page_subtitle">
					Consultoria e Assessoria
				</div>
			</div>
			<div class="tbb_breadcrumbs">
				<div class="container">
					<div class="breadcrumbs">
						<div class="breadcrumbs_inner">Você está em:
							<a href="index.html">Inicial</a>/
							<a href="index.html">Serviços e Soluções</a>/
							<a href="index.html">Consultoria e Assessoria</a>/
							<span class="current">Estratégia de Negócio</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="shard_section_dd">
		<div class="container">
			<div class="row">
				<div class="span6">
					<img src="images/servico-interna.jpg" alt="">
				</div>
				<div class="span1"></div>
				<div class="span5">
					<div class="content-servico">
						<h2>Estratégia de Negócio</h2>
						<p>Conceitos de serviços de TI, princípios de processo de atendimento e prestação de serviços, boas práticas para aumento de produtividade da equipe de atendimento.</p>
						<ul>
							<li>Redes sociais e serviços online;</li>
							<li>Gestão da Produção e Qualidade; </li>
							<li>Gestão e Administração do processo Produtivo;</li>
							<li>Gestão Empresarial (PME);</li>
							<li>Diagnóstico Empresarial;</li>
							<li>Gestão de TI;</li>
						</ul>
					</div>
					<div class="share">
						<button onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={URL}','name','width=600,height=400')" class="btn-facebook"><i class="icon-facebook"></i> Curtir</button>
						<button onclick="window.open('https://twitter.com/home?status={TÍTULO} - {URL}','name','width=600,height=400')" class="btn-twitter"><i class="icon-twitter"></i> Tweetar</button>
					</div>
					<a href="#" class="shard-button shard-button_blue shard-button_medium">Está interessado? <i class="icon-arrow-right2"></i></a>
				</div>
			</div>
		</div>
	</section>
	<section class="outros-servicos">
		<div class="container">
			<div class="row">
				<div class="span12">
					<h3 class="title">Veja mais soluções<br>
					em <strong>Consultoria e Assesoria</strong></h3>
				</div>
			</div>
			<div class="owl-servicos owl-carousel">
				<div class="item">
					<div class="item-servico">
						<a href="#">
							<img src="images/servico-estrategia.jpg" alt="">
							<span class="shadow"></span>
							<div class="content">
								<h4>Gestão de Portfólio</h4>
								<button class="shard-button">Saiba mais <i class="icon-arrow-right2"></i></button>
							</div>
						</a>
					</div>
				</div>
				<div class="item">
					<div class="item-servico">
						<a href="#">
							<img src="images/servico-inovacao.jpg" alt="">
							<span class="shadow"></span>
							<div class="content">
								<h4>Gestão de Portfólio</h4>
								<button class="shard-button">Saiba mais <i class="icon-arrow-right2"></i></button>
							</div>
						</a>
					</div>
				</div>
				<div class="item">
					<div class="item-servico">
						<a href="#">
							<img src="images/servico-processo.jpg" alt="">
							<span class="shadow"></span>
							<div class="content">
								<h4>Gestão de Portfólio</h4>
								<button class="shard-button">Saiba mais <i class="icon-arrow-right2"></i></button>
							</div>
						</a>
					</div>
				</div>
				<div class="item">
					<div class="item-servico">
						<a href="#">
							<img src="images/servico-usabilidade.jpg" alt="">
							<span class="shadow"></span>
							<div class="content">
								<h4>Gestão de Portfólio</h4>
								<button class="shard-button">Saiba mais <i class="icon-arrow-right2"></i></button>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="shard_section_dd no_padding contratar">
		<div class="shard_section_content">
			<div class="shard_container">
				<div class="shard_column_dd_span12 ">
					<div class="shard-callout_box ">
						<div class="shard_container">
							<div class="shard_column_dd_span2"></div>
							<div class="shard_column_dd_span5">
								<span class="shard-callout_box_title">Está interessado no treinamento? </span>
								<p>Envie sua mensagem para nossa equipe</p>
							</div>
							<div class="shard_column_dd_span2">
								<a href="#" target="_blank" class="shard-button shard-button_blue_light shard-button_small">
									Não deixe para depois
									<i class="icon-arrow-right2"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>