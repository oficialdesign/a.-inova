<?php include('header.php'); ?>

	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="tbb_title">
				<h1>Parceiros</h1>
			</div>
			<div class="tbb_breadcrumbs">
				<div class="container">
					<div class="breadcrumbs">
						<div class="breadcrumbs_inner">Você está em:
							<a href="index.html">Inicial</a>/
							<span class="current">Nossa Equipe</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="shard_section_dd">
		<div class="shard_container">
			<div class="shard_column_dd_span12">
				<p><strong>Em tecnologia e trabalho em equipe, networking e inovação são as palavras-chave para se obter resultados realmente eficazes.</strong></p>
				<p>Visando estes privilégio aos nossos clientes e parceiros, a A.Inova Consultores e Associados cultiva parcerias estratégicas nas áreas de tecnologia, comunicação e treinamentos, com empresas que contribuem com o nosso compromisso de oferecer sempre soluções com alta qualidade, tecnologia de ponta e que valorizem e venham acrescentar em todo e qualquer processo.</p>
			</div>
		</div>
		<div class="margin_bottom"></div>
		<div class="shard_container">
			<div class="box-parceiro">
				<div class="img">
					<img src="images/logo-nti.jpg" alt="">
				</div>
			</div>
			<div class="box-parceiro">
				<div class="img">
					<img src="images/logo-sebrae.jpg" alt="">
				</div>
			</div>
			<div class="box-parceiro">
				<div class="img">
					<img src="images/logo-wise.jpg" alt="">
				</div>
			</div>
			<div class="box-parceiro">
				<div class="img">
					<img src="images/logo-nti.jpg" alt="">
				</div>
			</div>
			<div class="box-parceiro">
				<div class="img">
					<img src="images/logo-sebrae.jpg" alt="">
				</div>
			</div>
			<div class="box-parceiro">
				<div class="img">
					<img src="images/logo-wise.jpg" alt="">
				</div>
			</div>
		</div>
		<div class="margin_bottom"></div>
		<div class="shard_section_content">
			<div class="shard_container aligncenter padding_reduced_both">
				<a href="javascript:history.go(-1);" class="shard-button shard-button_blue shard-button_medium icon-left">
					<i class="icon-arrow-left2"></i> Voltar
				</a>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>