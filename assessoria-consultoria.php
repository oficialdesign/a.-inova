<?php include('header.php'); ?>

	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="tbb_title">
				<h1>Consultoria e Assessoria</h1>
			</div>
			<div class="tbb_breadcrumbs">
				<div class="container">
					<div class="breadcrumbs">
						<div class="breadcrumbs_inner">Você está em:
							<a href="index.html">Inicial</a>/
							<a href="index.html">Serviços e Soluções</a>/
							<span class="current">Consultoria e ASsessoria</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="shard_section_dd">
		<div class="shard_container">
			<div class="shard_column_dd_span12 aligncenter">
				<p>A A.Inova oferece consultoria e assessoria nas áreas de Processo de desenvolvimento e serviços, Inovação, Usabilidade e Estratégia de Negócio, focando no  apoio de seus clientes na melhoria de processos organizacionais, aumentando a qualidade de seus produtos e serviços e na entrega de valor a seus clientes/usuário</p>
			</div>
		</div>
		<div class="margin_bottom"></div>
		<div class="shard_container">
			<div class="shard_column_dd_span3">
				<div class="box-servico">
					<a href="interna-servicos.php">
						<img src="images/servico-processo.jpg" alt="">
						<div class="shadow"></div>
						<h4>Processos</h4>
					</a>
				</div>
			</div>
			<div class="shard_column_dd_span3">
				<div class="box-servico">
					<a href="interna-servicos.php">
						<img src="images/servico-estrategia.jpg" alt="">
						<div class="shadow"></div>
						<h4>Estratégia de Inovação</h4>
					</a>
				</div>
			</div>
			<div class="shard_column_dd_span3">
				<div class="box-servico">
					<a href="interna-servicos.php">
						<img src="images/servico-usabilidade.jpg" alt="">
						<div class="shadow"></div>
						<h4>Usabilidade</h4>
					</a>
				</div>
			</div>
			<div class="shard_column_dd_span3">
				<div class="box-servico">
					<a href="interna-servicos.php">
						<img src="images/servico-inovacao.jpg" alt="">
						<div class="shadow"></div>
						<h4>Inovação</h4>
					</a>
				</div>
			</div>
		</div>
		<div class="shard_section_content">
			<div class="shard_container aligncenter padding_reduced_both">
				<a href="javascript:history.go(-1);" class="shard-button shard-button_blue shard-button_medium icon-left">
					<i class="icon-arrow-left2"></i> Voltar
				</a>
			</div>
		</div>
	</section>
	<section class="shard_section_dd no_padding contratar">
		<div class="shard_section_content">
			<div class="shard_container">
				<div class="shard_column_dd_span12 ">
					<div class="shard-callout_box ">
						<div class="shard_container">
							<div class="shard_column_dd_span2"></div>
							<div class="shard_column_dd_span5">
								<span class="shard-callout_box_title">Está interessado no treinamento? </span>
								<p>Envie sua mensagem para nossa equipe</p>
							</div>
							<div class="shard_column_dd_span2">
								<a href="#" target="_blank" class="shard-button shard-button_blue_light shard-button_small">
									Não deixe para depois
									<i class="icon-arrow-right2"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>