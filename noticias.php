<?php include('header.php'); ?>

	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="tbb_title">
				<h1>Notícias</h1>
			</div>
			<div class="tbb_breadcrumbs">
				<div class="container">
					<div class="breadcrumbs">
						<div class="breadcrumbs_inner">Você está em:
							<a href="index.html">Inicial</a>/
							<span class="current">Notícias</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="blog_category_index span9 content_with_right_sidebar">
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-pen"></i>
								<span class="post_date">
									<span class="post_day">01 Oct</span>
									<span class="post_our_minute">12:03</span>
									<span class="post_am_pm">pm</span>
								</span>
							</div>
							<div class="post_main post_main_default">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">Standard Blog Post with Image</a>
									</h3>
									<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <strong>bibendum auctor</strong>, nisi elit consequat ipsum, nec sagittis sem nibh id elit. </p>
								</div>
								<a href="interna-noticias.php">
									<img width="1170" height="628" src="images/post11.jpg" alt="post1">
								</a>						
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-pen"></i>
								<span class="post_date">
									<span class="post_day">28 Aug</span>
									<span class="post_our_minute">08:16</span>
									<span class="post_am_pm">am</span>
								</span>
							</div>
							<div class="post_main post_main_default">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">A steady Stream of Madness</a>
									</h3>
									<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis 
										<a href="#" title="TriState" target="_blank">Tristate</a> nisi elit consequat ipsum, nec sagittis sem nibh id elit.
									</p>
								</div>
								<a href="interna-noticias.php">
									<img width="1170" height="628" src="images/post2.jpg" alt="post2">
								</a>							
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-play"></i>
								<span class="post_date">
									<span class="post_day">27 Aug</span>
									<span class="post_our_minute">08:12</span>
									<span class="post_am_pm">am</span>
								</span>
							</div>
							<div class="post_main post_main_youtube">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">Jean-Louis Nguyen Reviews 2012 in this Astonishing Video</a>
									</h3>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna <strong>aliquam erat</strong> volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.
									</p>
								</div>
								<div class="videoWrapper-youtube">
									<iframe src="http://www.youtube.com/embed/TYopqtXMSwY?showinfo=0&amp;autohide=1&amp;related=0" allowfullscreen=""></iframe>
								</div>											
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-headphonesthree"></i>
								<span class="post_date">
									<span class="post_day">26 Aug</span>
									<span class="post_our_minute">08:12</span>
									<span class="post_am_pm">am</span>
								</span>
							</div>
							<div class="post_main post_main_soundcloud">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">Al Gore on Climate Change</a>
									</h3>
									<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <strong>bibendum auctor</strong>, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
									</p>
								</div>
								<iframe class="width_100" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F77976856">
								</iframe>							
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-pen"></i>
								<span class="post_date">
									<span class="post_day">22 Aug</span>
									<span class="post_our_minute">12:09</span>
									<span class="post_am_pm">pm</span>
								</span>
							</div>
							<div class="post_main post_main_default">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">This is post Excerpt, not title. You can use it to draw attention or describe it in detailed way</a>
									</h3>
									<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <strong>bibendum auctor</strong>, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
									</p>
								</div>
								<a href="interna-noticias.php">
									<img width="1170" height="628" src="images/post5.jpg" alt="post5">
								</a>											
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-play"></i>
								<span class="post_date">
									<span class="post_day">12 Aug</span>
									<span class="post_our_minute">08:12</span>
									<span class="post_am_pm">am</span>
								</span>
							</div>
							<div class="post_main post_main_vimeo">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">Featured Blog Post Template Excerpt to Catch Readers Attention and Describe the Story in Short</a>
									</h3>
									<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <strong>bibendum auctor</strong>, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
									</p>
								</div>
								<div class="videoWrapper-vimeo"><iframe src="http://player.vimeo.com/video/79426902?title=0&amp;byline=0&amp;portrait=0" allowfullscreen=""></iframe></div>											
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-pen"></i>
								<span class="post_date">
									<span class="post_day">28 Jul</span>
									<span class="post_our_minute">20:10</span>
									<span class="post_am_pm">pm</span>
								</span>
							</div>
							<div class="post_main post_main_default">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">Deal action pushes S&amp;P higher; Nasdaq  extends winning streak</a>
									</h3>
									<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <strong>bibendum auctor</strong>, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
									</p>
								</div>
								<a href="interna-noticias.php">
									<img width="1170" height="628" src="images/post6.jpg" alt="post6">
								</a>											
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-pen"></i>
								<span class="post_date">
									<span class="post_day">18 Jul</span>
									<span class="post_our_minute">12:58</span>
									<span class="post_am_pm">pm</span>
								</span>
							</div>
							<div class="post_main post_main_default">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">Regular Blog Post</a>
									</h3>
									<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <strong>bibendum auctor</strong>, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
									</p>
								</div>
								<a href="interna-noticias.php">
									<img width="1170" height="501" src="images/blog2-1.jpg" alt="blog2 (1)">
								</a>											
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-play"></i>
								<span class="post_date">
									<span class="post_day">14 Jun</span>
									<span class="post_our_minute">08:53</span>
									<span class="post_am_pm">am</span>
								</span>
							</div>
							<div class="post_main post_main_youtube">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">Featured Blog Post Template Excerpt to Catch Readers Attention and Describe the Story in Short</a>
									</h3>
									<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <strong>bibendum auctor</strong>, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
									</p>
								</div>
								<div class="videoWrapper-youtube">
									<iframe src="http://www.youtube.com/embed/LStXdttFj_o?showinfo=0&amp;autohide=1&amp;related=0" allowfullscreen=""></iframe>
								</div>											
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post post_wrapper clearfix">
						<div class="post_content">
							<div class="post_badges">
								<i class="icon-headphonesthree"></i>
								<span class="post_date">
									<span class="post_day">02 Jun</span>
									<span class="post_our_minute">08:35</span>
									<span class="post_am_pm">am</span>
								</span>
							</div>
							<div class="post_main post_main_soundcloud">
								<div class="post_main_inner">
									<h3 class="post_main_title">
										<a href="interna-noticias.php">Featured Blog Post Template Excerpt to Catch Readers Attention and Describe the Story in Short</a>
									</h3>
									<p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <strong>bibendum auctor</strong>, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
									</p>
								</div>
								<iframe class="width_100" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F77976856"></iframe>								
								<div class="post-readmore">
									<a href="interna-noticias.php#comments_section" class="post_meta_comments">Comente</a>
									<span>•</span>
									<a href="interna-noticias.php" class="more-link">Saiba mais</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php include('sidebar.php'); ?>
			</div>
			<div class="row">
				<div class="span12">
					<section id="blog_pagination" class="clearfix">
						<div class="pagination">
							<span class="blog_pagexofy left">Page 1 of 2</span>
							<span class="page-numbers current">1</span>
							<a class="page-numbers" href="blog-right-sidebar.html">2</a> 
							<a class="next page-numbers" href="blog-right-sidebar.html"><i class="icon-chevronright"></i></a>
						</div>
					</section>
				</div>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>